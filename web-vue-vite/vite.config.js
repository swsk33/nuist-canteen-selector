import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

export default defineConfig({
	base: '/nuist-canteen/',
	plugins: [
		vue(),
		AutoImport({
			resolvers: [ElementPlusResolver()]
		}),
		Components({
			resolvers: [ElementPlusResolver()]
		})],
	define: {
		'process.env': {}
	},
	server: {
		host: '0.0.0.0',
		// 跨域配置
		proxy: {
			// 请求代理
			'/api': {
				target: 'http://127.0.0.1:8800/',
				changeOrigin: true
			}
		}
	}
});