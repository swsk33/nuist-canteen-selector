import { createApp } from 'vue';
import { createPinia } from 'pinia';

// 根组件
import App from './App.vue';

// 导入css
import 'element-plus/dist/index.css';
import 'leaflet/dist/leaflet.css';

// 创建实例并挂载
createApp(App).use(createPinia()).mount('#app');