// pinia-定位store
import { defineStore } from 'pinia';
import { REQUEST_METHOD, sendRequest } from '../utils/request';
import { ElNotification } from 'element-plus';
import { useMapStore } from './map.js';

export const useLocationStore = defineStore('location', {
	state() {
		return {
			/**
			 * 当前位置，第一个元素表示经度，第二个元素表示纬度
			 */
			currentPosition: {
				/**
				 * 经度
				 */
				longitude: 118.708082,
				/**
				 * 纬度
				 */
				latitude: 32.200014
			},
			/**
			 * 定位是否完成
			 */
			positionOk: false
		};
	},
	actions: {
		/**
		 * WGS84坐标系转火星坐标系
		 * @param {*} lon 原始经度
		 * @param {*} lat 原始纬度
		 */
		async GPSToGao(lon, lat) {
			const response = await sendRequest('/api/map/wgs-to-gaode/longitude/' + lon + '/latitude/' + lat, REQUEST_METHOD.GET);
			if (!response.success) {
				return;
			}
			const resultJson = JSON.parse(response.data);
			this.currentPosition.longitude = resultJson.result[0].x;
			this.currentPosition.latitude = resultJson.result[0].y;
		},
		/**
		 * 获取自身位置
		 */
		getCurrentPosition() {
			// 其它pinia模块
			const mapStore = useMapStore();
			// 定位成功后执行
			const successCallback = async (position) => {
				this.positionOk = true;
				await this.GPSToGao(position.coords.longitude, position.coords.latitude);
				// 设定标记
				mapStore.currentMarker.setLatLng([this.currentPosition.latitude, this.currentPosition.longitude]);
			};
			// 定位失败后执行
			const failedCallback = () => {
				this.positionOk = false;
				ElNotification({
					title: '错误',
					message: '定位失败！请开启定位！',
					type: 'error',
					duration: 1000
				});
			};
			navigator.geolocation.getCurrentPosition(successCallback, failedCallback);
		}
	}
});