// pinia-地图store
import { defineStore } from 'pinia';
import L from 'leaflet';
import marker from '../assets/image/current_marker.png';
import { useLocationStore } from './location.js';

export const useMapStore = defineStore('map', {
	state() {
		return {
			/**
			 * 地图对象
			 */
			map: undefined,
			/**
			 * 当前位置标记
			 */
			currentMarker: undefined
		};
	},
	actions: {
		/**
		 * 初始化地图容器
		 * @param containerId 用于存放地图的元素的id
		 */
		initMap(containerId) {
			// 初始化地图并加入图层
			this.map = L.map('map', {
				center: [32.200014, 118.708082],
				// maxZoom: 18,
				// minZoom: 16,
				zoom: 17,
				zoomControl: false
			});
			// 瓦片图服务url
			const mapUrl = 'https://wprd04.is.autonavi.com/appmaptile?lang=zh_cn&size=1&style=7&x={x}&y={y}&z={z}';
			L.tileLayer(mapUrl).addTo(this.map);
			// 初始化标记并加入地图
			this.currentMarker = L.marker([32.204584, 118.718511], {
				icon: L.icon({
					iconUrl: marker,
					iconSize: [27, 36],
					iconAnchor: [14, 35]
				})
			});
			this.currentMarker.addTo(this.map);
			// 添加监听事件
			this.map.on('click', (e) => {
				L.popup().setLatLng(e.latlng).setContent('经度：' + e.latlng.lng.toFixed(6) + ' 纬度：' + e.latlng.lat.toFixed(6)).openOn(this.map);
			});
		},
		/**
		 * 把视野聚焦至中心
		 */
		setToCenter() {
			const locationStore = useLocationStore();
			this.map.setView([locationStore.currentPosition.latitude, locationStore.currentPosition.longitude], 17, {
				animate: true
			});
		}
	}
});