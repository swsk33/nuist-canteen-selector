// pinia-餐厅store
import { defineStore } from 'pinia';
import { REQUEST_METHOD, sendRequest } from '../utils/request';
import { ElNotification } from 'element-plus';
import { useLocationStore } from './location.js';
import L from 'leaflet';

export const useCanteenStore = defineStore('canteen', {
	state() {
		return {
			/**
			 * 全部餐厅列表
			 */
			canteens: [],
			/**
			 * 最近的餐厅
			 */
			nearest: undefined
		};
	},
	actions: {
		/**
		 * 获取全部餐厅列表，并计算餐厅的距离
		 */
		async getAllCanteens() {
			const response = await sendRequest('/api/canteen/get-all', REQUEST_METHOD.GET);
			if (!response.success) {
				ElNotification({
					title: '错误',
					message: '获取餐厅信息失败！',
					type: 'error',
					duration: 1000
				});
				return;
			}
			this.canteens = response.data;
		},
		/**
		 * 获取最近的餐厅
		 */
		getNearest() {
			// 开始计算距离
			const locationStore = useLocationStore();
			locationStore.getCurrentPosition();
			for (let item of this.canteens) {
				item.distance = L.latLng(locationStore.currentPosition.latitude, locationStore.currentPosition.longitude).distanceTo(L.latLng(item.latitude, item.longitude));
			}
			// 选择最近餐厅
			let canteen = this.canteens[0];
			for (let i = 1; i < this.canteens.length; i++) {
				if (this.canteens[i].distance < canteen.distance) {
					canteen = this.canteens[i];
				}
			}
			this.nearest = canteen;
		}
	}
});