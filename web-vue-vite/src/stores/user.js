// pinia-用户store
import { defineStore } from 'pinia';
import { REQUEST_METHOD, sendRequest } from '../utils/request';
import { ElNotification } from 'element-plus';

export const useUserStore = defineStore('user', {
	state() {
		return {
			/**
			 * 用户数据
			 */
			userData: undefined,
			/**
			 * 用户是否登录
			 */
			isLogin: false
		};
	},
	actions: {
		/**
		 * 检查用户是否登录
		 */
		async checkLogin() {
			const response = await sendRequest('/api/user/is-login', REQUEST_METHOD.GET);
			if (!response.success) {
				this.isLogin = false;
				this.userData = undefined;
				return;
			}
			this.userData = response.data;
			this.isLogin = true;
		},
		/**
		 * 用户登录
		 * @param loginData 用于登录的请求体
		 * @return 是否登录成功
		 */
		async userLogin(loginData) {
			const response = await sendRequest('/api/user/login', REQUEST_METHOD.POST, loginData);
			if (!response.success) {
				ElNotification({
					title: '登录失败',
					message: response.message,
					type: 'error',
					duration: 1000
				});
				return false;
			}
			ElNotification({
				title: '成功',
				message: '登录成功！',
				type: 'success',
				duration: 1000
			});
			this.userData = response.data;
			this.isLogin = true;
			return true;
		},
		/**
		 * 用户注册
		 * @param registerData 注册请求体数据
		 * @return 是否注册成功
		 */
		async userRegister(registerData) {
			const response = await sendRequest('/api/user/register', REQUEST_METHOD.POST, registerData);
			if (!response.success) {
				ElNotification({
					title: '注册失败',
					message: response.message,
					type: 'error',
					duration: 1000
				});
				return false;
			}
			ElNotification({
				title: '成功',
				message: '注册成功！',
				type: 'success',
				duration: 1000
			});
			this.userData = response.data;
			this.isLogin = true;
			return true;
		},
		/**
		 * 用户数据更新
		 * @param updateData 用于用户更新的请求体
		 * @return 是否修改成功
		 */
		async userUpdate(updateData) {
			const response = await sendRequest('/api/user/update', REQUEST_METHOD.PUT, updateData);
			if (!response.success) {
				ElNotification({
					title: '更新数据失败',
					message: response.message,
					type: 'error',
					duration: 1000
				});
				return false;
			}
			this.userData = response.data;
			return true;
		},
		/**
		 * 退出登录
		 */
		async logout() {
			const response = await sendRequest('/api/user/logout', REQUEST_METHOD.GET);
			if (!response.success) {
				return;
			}
			ElNotification({
				title: '成功',
				message: '退出登录成功！',
				type: 'success',
				duration: 1000
			});
			this.userData = undefined;
			this.isLogin = false;
		}
	}
});