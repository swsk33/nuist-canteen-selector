// pinia-选择结果store
import { defineStore } from 'pinia';

export const useSelectStore = defineStore('select', {
	state() {
		return {
			result: {
				canteenName: undefined,
				storeName: undefined
			}
		};
	}
});