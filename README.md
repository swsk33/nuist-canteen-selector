# 南信大-食堂选择器

## 1，介绍

大学生活相比中学是丰富多彩的，充满活力的，大量新鲜信息的涌入同时也造就了不少“选择困难症”，例如身边总有同学到了饭点便会纠结今天该吃什么好。基于这种背景下，我们决定结合GIS技术，研发出这样一款食堂选择的小型平台，让同学动动手指，几秒钟便能找到自己心仪的食堂和饭菜。

## 2，技术架构

前端使用Vue3框架：

- Vue3 + Vite 构建
- Pinia 状态管理
- element-plus 组件库
- sass 预处理器
- axios 网络请求
- leaflet WebGIS-地图呈现

后端使用Spring Boot + MyBatisPlus：

- MyBatisPlus ORM框架
- Sa-Token 鉴权和认证
- Okhttps 网络库
- Commons-lang 实用类

## 3，部署

### (1) 初始化MySQL数据库

首先安装并配置好MySQL数据库，或者准备一台现成的MySQL服务器，并连接，执行下列SQL创建必要的数据库：

```sql
create database `nuistcanteen`;
use `nuistcanteen`;
```

然后下载这个SQL文件，并使用`source`命令执行这个SQL文件：[下载地址](https://gitee.com/swsk33/nuist-canteen-selector/raw/master/server-springboot/sql/init.sql)

### (2) 拉取Docker镜像

```bash
docker pull swsk33/nuist-canteen-selector
```

### (3) 创建数据卷

```bash
docker volume create nuistcanteen-springboot-config
docker volume create nuistcanteen-nginx-config
```

分别存放容器的Spring Boot和Nginx配置。

### (4) 创建并启动容器

```bash
docker run -id --name=nuist-canteen -v nuistcanteen-springboot-config:/app/config -v nuistcanteen-nginx-config:/usr/local/nginx/conf -p 80:80 -p 443:443 swsk33/nuist-canteen-selector
```

根据上述具名挂载的配置，两个配置文件分别挂载在如下位置：

- Spring Boot配置：`/var/lib/docker/volumes/nuistcanteen-springboot-config/_data`
- Nginx配置：`/var/lib/docker/volumes/nuistcanteen-nginx-config/_data`

### (5) 修改配置

编辑Spring Boot配置文件，是`YAML`格式，位于上述配置挂载文件夹中的`application.yml`：

#### ① 数据库

- `spring.datasource.url` 把`127.0.0.1:3306`这部分替换为自己的数据库地址
- `spring.datasource.username` 数据库用户名
- `spring.datasource.password` 数据库密码

#### ② 百度地图开发者API Key

本后台需要调用百度地图API进行坐标系的转换，因此需要先自行去百度地图开发者平台申请免费的API Key，然后填写至字段`com.gitee.swsk33.nuist-canteen.baidu-map-key`。

#### ③ 开启Https

修改**Nginx配置**目录中的`nginx.conf`配置即可，至于Nginx配置https此处不再赘述。

全部修改完成请重启容器：

```bash
docker restart nuist-canteen
```

最后，访问`你的服务器地址或域名/nuist-canteen`即可。

> 最后更新：2023.1.17
