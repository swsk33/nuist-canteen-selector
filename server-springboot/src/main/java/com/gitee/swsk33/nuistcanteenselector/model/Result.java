package com.gitee.swsk33.nuistcanteenselector.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 请求结果类
 *
 * @param <T> 数据体类型
 */
@Data
public class Result<T> implements Serializable {

	/**
	 * 消息
	 */
	private String message;

	/**
	 * 是否成功
	 */
	private boolean success;

	/**
	 * 数据体
	 */
	private T data;

	/**
	 * 设定结果成功
	 *
	 * @param msg 消息
	 */
	public void setResultSuccess(String msg) {
		this.message = msg;
		this.data = null;
		this.success = true;
	}

	/**
	 * 设定结果成功
	 *
	 * @param msg  消息
	 * @param data 数据体
	 */
	public void setResultSuccess(String msg, T data) {
		this.message = msg;
		this.data = data;
		this.success = true;
	}

	/**
	 * 设定结果失败
	 *
	 * @param msg 消息
	 */
	public void setResultFailed(String msg) {
		this.message = msg;
		this.data = null;
		this.success = false;
	}

}