package com.gitee.swsk33.nuistcanteenselector.api;

import com.gitee.swsk33.nuistcanteenselector.model.Result;
import com.gitee.swsk33.nuistcanteenselector.service.MapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/map")
public class MapAPI {

	@Autowired
	private MapService mapService;

	@GetMapping("/wgs-to-gaode/longitude/{longitude}/latitude/{latitude}")
	public Result<String> WGS84ToGaode(@PathVariable Double longitude, @PathVariable Double latitude) {
		return mapService.WGS84ToGaode(longitude, latitude);
	}

}