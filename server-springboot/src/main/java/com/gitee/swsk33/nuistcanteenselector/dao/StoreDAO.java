package com.gitee.swsk33.nuistcanteenselector.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.swsk33.nuistcanteenselector.dataobject.Store;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StoreDAO extends BaseMapper<Store> {

	@Select("select * from `store` where canteen_id = #{canteenId} and type = #{type}")
	List<Store> getByCanteenAndType(int canteenId, String type);

}