package com.gitee.swsk33.nuistcanteenselector.service;

import com.gitee.swsk33.nuistcanteenselector.model.Result;
import org.springframework.stereotype.Service;

@Service
public interface MapService {

	/**
	 * 调用百度地图API，将WGS84坐标系转换成高德地图坐标系
	 *
	 * @param longitude 经度
	 * @param latitude  纬度
	 * @return API返回的JSON结果
	 */
	Result<String> WGS84ToGaode(Double longitude, Double latitude);

}