package com.gitee.swsk33.nuistcanteenselector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CanteenSelectorApplication {

	public static void main(String[] args) {
		System.setProperty("spring.config.location", "file:config/");
		SpringApplication.run(CanteenSelectorApplication.class, args);
	}

}