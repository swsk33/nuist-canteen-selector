package com.gitee.swsk33.nuistcanteenselector.dataobject;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 餐厅类
 */
@Data
public class Canteen implements Serializable {

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;

	/**
	 * 餐厅名
	 */
	private String name;

	/**
	 * 初始分
	 */
	private Integer initScore;

	/**
	 * 平均价格
	 */
	private Integer averagePrice;

	/**
	 * 经度
	 */
	private Double longitude;

	/**
	 * 纬度
	 */
	private Double latitude;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreated;

	/**
	 * 修改时间
	 */
	private LocalDateTime gmtModified;

}