package com.gitee.swsk33.nuistcanteenselector.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.swsk33.nuistcanteenselector.dataobject.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserDAO extends BaseMapper<User> {

	@Select("select * from `user` where username = #{username}")
	User getByUsername(String username);

}