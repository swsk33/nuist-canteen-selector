package com.gitee.swsk33.nuistcanteenselector.service;

import com.gitee.swsk33.nuistcanteenselector.dataobject.Store;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StoreService {

	/**
	 * 通过餐厅和菜系筛选店家
	 */
	Result<List<Store>> getByCanteenAndType(Integer canteenId, String type);

}