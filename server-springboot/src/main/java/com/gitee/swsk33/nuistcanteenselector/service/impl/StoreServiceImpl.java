package com.gitee.swsk33.nuistcanteenselector.service.impl;

import com.gitee.swsk33.nuistcanteenselector.dao.StoreDAO;
import com.gitee.swsk33.nuistcanteenselector.dataobject.Store;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import com.gitee.swsk33.nuistcanteenselector.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StoreServiceImpl implements StoreService {

	@Autowired
	private StoreDAO storeDAO;

	@Override
	public Result<List<Store>> getByCanteenAndType(Integer canteenId, String type) {
		Result<List<Store>> result = new Result<>();
		List<Store> stores = storeDAO.getByCanteenAndType(canteenId, type);
		if (stores == null || stores.size() == 0) {
			result.setResultFailed("找不到符合条件的店家！");
			return result;
		}
		result.setResultSuccess("查找店家成功！", stores);
		return result;
	}

}