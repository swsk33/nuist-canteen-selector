package com.gitee.swsk33.nuistcanteenselector.service;

import com.gitee.swsk33.nuistcanteenselector.dataobject.User;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

	/**
	 * 用户注册
	 */
	Result<User> register(User user);

	/**
	 * 用户登录
	 */
	Result<User> login(User user);

	/**
	 * 修改用户信息
	 */
	Result<User> update(User user) throws Exception;

}