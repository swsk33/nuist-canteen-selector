package com.gitee.swsk33.nuistcanteenselector.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.swsk33.nuistcanteenselector.dataobject.Canteen;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CanteenDAO extends BaseMapper<Canteen> {

	@Select("select distinct `canteen`.* from `store` inner join `canteen` on `store`.canteen_id = `canteen`.id where `store`.type = #{type}")
	List<Canteen> getByType(String type);

	@Select("select distinct `canteen`.* from `store` inner join `canteen` on `store`.canteen_id = `canteen`.id where `store`.type = #{type} and average_price >= #{min} and average_price <= #{max};")
	List<Canteen> getByTypeAndPrice(String type, Integer min, Integer max);

}