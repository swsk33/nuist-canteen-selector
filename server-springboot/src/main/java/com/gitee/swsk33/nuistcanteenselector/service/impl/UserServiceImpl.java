package com.gitee.swsk33.nuistcanteenselector.service.impl;


import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.gitee.swsk33.nuistcanteenselector.dao.UserDAO;
import com.gitee.swsk33.nuistcanteenselector.dataobject.User;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import com.gitee.swsk33.nuistcanteenselector.param.CommonValue;
import com.gitee.swsk33.nuistcanteenselector.service.UserService;
import com.gitee.swsk33.nuistcanteenselector.util.BCryptEncoder;
import com.gitee.swsk33.nuistcanteenselector.util.ClassExamine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Objects;

@Component
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public Result<User> register(User user) {
		Result<User> result = new Result<>();
		User getUser = userDAO.getByUsername(user.getUsername());
		if (getUser != null) {
			result.setResultFailed("用户名已存在！");
			return result;
		}
		// 加密保存密码
		user.setPassword(BCryptEncoder.encode(user.getPassword()));
		userDAO.insert(user);
		// 登录并缓存对象
		StpUtil.login(user.getId());
		getUser = userDAO.selectById(user.getId());
		StpUtil.getSession().set(CommonValue.SESSION_NAME, getUser);
		result.setResultSuccess("注册成功！", getUser);
		return result;
	}

	@Override
	public Result<User> login(User user) {
		Result<User> result = new Result<>();
		User getUser = userDAO.getByUsername(user.getUsername());
		if (getUser == null) {
			result.setResultFailed("用户名不存在！");
			return result;
		}
		if (!BCryptEncoder.check(user.getPassword(), getUser.getPassword())) {
			result.setResultFailed("用户名或者密码错误！");
			return result;
		}
		// 登录并缓存对象
		StpUtil.login(getUser.getId());
		StpUtil.getSession().set(CommonValue.SESSION_NAME, getUser);
		result.setResultSuccess("登录成功！", getUser);
		return result;
	}

	@Override
	@SaCheckLogin
	public Result<User> update(User user) throws Exception {
		Result<User> result = new Result<>();
		User getUser = (User) StpUtil.getSession().get(CommonValue.SESSION_NAME);
		if (!Objects.equals(getUser.getId(), user.getId())) {
			result.setResultFailed("不能修改别人的信息！");
			return result;
		}
		// 对象堆叠
		ClassExamine.objectOverlap(user, getUser);
		// 密码修改则重新加密
		if (!user.getPassword().equals(getUser.getPassword())) {
			user.setPassword(BCryptEncoder.encode(user.getPassword()));
		}
		// 时间
		user.setGmtModified(LocalDateTime.now());
		userDAO.updateById(user);
		// 刷新登录缓存
		User newUser = userDAO.selectById(user.getId());
		StpUtil.getSession().set(CommonValue.SESSION_NAME, newUser);
		result.setResultSuccess("修改信息成功！", newUser);
		return result;
	}

}