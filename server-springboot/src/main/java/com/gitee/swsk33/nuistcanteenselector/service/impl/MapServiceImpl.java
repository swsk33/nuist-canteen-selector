package com.gitee.swsk33.nuistcanteenselector.service.impl;

import com.ejlchina.okhttps.HTTP;
import com.gitee.swsk33.nuistcanteenselector.config.ConfigProperties;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import com.gitee.swsk33.nuistcanteenselector.service.MapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MapServiceImpl implements MapService {

	@Autowired
	private ConfigProperties configProperties;

	/**
	 * 用于网络请求的对象
	 */
	private final HTTP httpClient = HTTP.builder().build();

	@Override
	public Result<String> WGS84ToGaode(Double longitude, Double latitude) {
		Result<String> result = new Result<>();
		String url = "https://api.map.baidu.com/geoconv/v1/?coords=" + longitude + "," + latitude + "&from=1&to=3&ak=" + configProperties.getBaiduMapKey();
		String requestResult = httpClient.sync(url).get().getBody().toString();
		result.setResultSuccess("请求完成！", requestResult);
		return result;
	}

}