package com.gitee.swsk33.nuistcanteenselector.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "com.gitee.swsk33.nuist-canteen")
@Component
public class ConfigProperties {

	/**
	 * 百度地图开发者API Key
	 */
	private String baiduMapKey;

}