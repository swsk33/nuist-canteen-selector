package com.gitee.swsk33.nuistcanteenselector.service.impl;

import com.gitee.swsk33.nuistcanteenselector.dao.CanteenDAO;
import com.gitee.swsk33.nuistcanteenselector.dataobject.Canteen;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import com.gitee.swsk33.nuistcanteenselector.service.CanteenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CanteenServiceImpl implements CanteenService {

	@Autowired
	private CanteenDAO canteenDAO;

	@Override
	public Result<List<Canteen>> getByType(String type) {
		Result<List<Canteen>> result = new Result<>();
		List<Canteen> canteens = canteenDAO.getByType(type);
		if (canteens == null || canteens.size() == 0) {
			result.setResultFailed("没有符合条件的餐厅！");
			return result;
		}
		result.setResultSuccess("获取餐厅成功！", canteens);
		return result;
	}

	@Override
	public Result<List<Canteen>> getByTypeAndPrice(String type, Integer min, Integer max) {
		Result<List<Canteen>> result = new Result<>();
		List<Canteen> canteens = canteenDAO.getByTypeAndPrice(type, min, max);
		if (canteens == null || canteens.size() == 0) {
			result.setResultFailed("没有符合条件的餐厅！");
			return result;
		}
		result.setResultSuccess("获取餐厅成功！", canteens);
		return result;
	}

	@Override
	public Result<List<Canteen>> getAll() {
		Result<List<Canteen>> result = new Result<>();
		List<Canteen> canteens = canteenDAO.selectList(null);
		result.setResultSuccess("查找完成！", canteens);
		return result;
	}

}