package com.gitee.swsk33.nuistcanteenselector.param;

/**
 * 校验规则分组
 */
public class ValidGroup {

	/**
	 * 添加用户规则
	 */
	public interface UserAdd {
	}

	/**
	 * 更新用户规则
	 */
	public interface UserUpdate {
	}

}