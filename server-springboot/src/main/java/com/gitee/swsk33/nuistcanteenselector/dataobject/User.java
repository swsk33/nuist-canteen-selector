package com.gitee.swsk33.nuistcanteenselector.dataobject;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gitee.swsk33.nuistcanteenselector.param.ValidGroup;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;


import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户类
 */
@Data
@JsonIgnoreProperties(allowSetters = true, value = {"password"})
public class User implements Serializable {

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	@NotNull(groups = ValidGroup.UserUpdate.class, message = "用户id不能为空！")
	private Integer id;

	/**
	 * 用户名
	 */
	@NotEmpty(groups = ValidGroup.UserAdd.class, message = "用户名不能为空！")
	@Size(groups = {ValidGroup.UserAdd.class, ValidGroup.UserUpdate.class}, max = 16, message = "用户名长度不能超过16！")
	private String username;

	/**
	 * 密码
	 */
	@Size(groups = {ValidGroup.UserAdd.class, ValidGroup.UserUpdate.class}, min = 8, message = "密码长度不能小于8！")
	@NotEmpty(groups = ValidGroup.UserAdd.class, message = "密码不能为空！")
	private String password;

	/**
	 * 昵称
	 */
	@Size(groups = {ValidGroup.UserAdd.class, ValidGroup.UserUpdate.class}, max = 32, message = "昵称长度不能超过32！")
	private String nickname;

	/**
	 * 餐厅选择记录JSON数据
	 * 形式为：{"餐厅1名字":"餐厅1被选择次数","餐厅2名字":"餐厅2被选择次数",...}
	 */
	@TableField("canteen_record_json")
	private String canteenRecordJSON = "{}";

	/**
	 * 菜系选择记录JSON数据
	 * 形式为：{"菜系1名字":"菜系1被选择次数","菜系2名字":"菜系2被选择次数",...}
	 */
	@TableField("type_record_json")
	private String typeRecordJSON = "{}";

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreated;

	/**
	 * 修改时间
	 */
	private LocalDateTime gmtModified;

}