package com.gitee.swsk33.nuistcanteenselector.service;

import com.gitee.swsk33.nuistcanteenselector.dataobject.Canteen;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CanteenService {

	/**
	 * 根据菜系获取食堂
	 */
	Result<List<Canteen>> getByType(String type);

	/**
	 * 根据菜系和价格区间获取食堂
	 */
	Result<List<Canteen>> getByTypeAndPrice(String type, Integer min, Integer max);

	/**
	 * 获取全部餐厅
	 */
	Result<List<Canteen>> getAll();

}