package com.gitee.swsk33.nuistcanteenselector.api;

import cn.dev33.satoken.stp.StpUtil;
import com.gitee.swsk33.nuistcanteenselector.dataobject.User;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import com.gitee.swsk33.nuistcanteenselector.param.CommonValue;
import com.gitee.swsk33.nuistcanteenselector.param.ValidGroup;
import com.gitee.swsk33.nuistcanteenselector.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserAPI {

	@Autowired
	private UserService userService;

	@PostMapping("/login")
	public Result<User> login(@RequestBody @Valid User user, BindingResult errors) {
		if (errors.hasErrors()) {
			Result<User> result = new Result<>();
			result.setResultFailed(errors.getFieldError().getDefaultMessage());
			return result;
		}
		return userService.login(user);
	}

	@PostMapping("/register")
	public Result<User> register(@RequestBody @Validated(ValidGroup.UserAdd.class) User user, BindingResult errors) {
		if (errors.hasErrors()) {
			Result<User> result = new Result<>();
			result.setResultFailed(errors.getFieldError().getDefaultMessage());
			return result;
		}
		return userService.register(user);
	}

	@GetMapping("/logout")
	public Result<Void> logout() {
		Result<Void> result = new Result<>();
		StpUtil.logout();
		result.setResultSuccess("退出登录成功！");
		return result;
	}

	@GetMapping("/is-login")
	public Result<User> isLogin() {
		Result<User> result = new Result<>();
		if (!StpUtil.isLogin()) {
			result.setResultFailed("用户未登录！");
			return result;
		}
		User getUser = (User) StpUtil.getSession().get(CommonValue.SESSION_NAME);
		result.setResultSuccess("用户已登录！", getUser);
		return result;
	}

	@PutMapping("/update")
	public Result<User> update(@RequestBody @Validated(ValidGroup.UserUpdate.class) User user, BindingResult errors) throws Exception {
		if (errors.hasErrors()) {
			Result<User> result = new Result<>();
			result.setResultFailed(errors.getFieldError().getDefaultMessage());
			return result;
		}
		return userService.update(user);
	}

}