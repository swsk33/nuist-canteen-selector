package com.gitee.swsk33.nuistcanteenselector.dataobject;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 店铺类
 */
@Data
public class Store implements Serializable {

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;

	/**
	 * 店铺名
	 */
	private String name;

	/**
	 * 店铺菜系
	 */
	private String type;

	/**
	 * 所属餐厅id
	 */
	private int canteenId;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreated = LocalDateTime.now();

	/**
	 * 修改时间
	 */
	private LocalDateTime gmtModified = LocalDateTime.now();

}