package com.gitee.swsk33.nuistcanteenselector.api;

import com.gitee.swsk33.nuistcanteenselector.dataobject.Store;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import com.gitee.swsk33.nuistcanteenselector.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/store")
public class StoreAPI {

	@Autowired
	private StoreService storeService;

	@GetMapping("/get-canteen-type/canteen/{canteenId}/type/{type}")
	public Result<List<Store>> getStore(@PathVariable Integer canteenId, @PathVariable String type) {
		return storeService.getByCanteenAndType(canteenId, type);
	}

}