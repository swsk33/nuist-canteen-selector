package com.gitee.swsk33.nuistcanteenselector.api;

import com.gitee.swsk33.nuistcanteenselector.dataobject.Canteen;
import com.gitee.swsk33.nuistcanteenselector.model.Result;
import com.gitee.swsk33.nuistcanteenselector.service.CanteenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/canteen")
public class CanteenAPI {

	@Autowired
	private CanteenService canteenService;

	@GetMapping("/get-by-type/{type}")
	public Result<List<Canteen>> getByType(@PathVariable String type) {
		return canteenService.getByType(type);
	}

	@GetMapping("/get-by-type-price/type/{type}/min/{min}/max/{max}")
	public Result<List<Canteen>> getByTypeAndPrice(@PathVariable String type, @PathVariable int min, @PathVariable int max) {
		return canteenService.getByTypeAndPrice(type, min, max);
	}

	@GetMapping("/get-all")
	public Result<List<Canteen>> getAll() {
		return canteenService.getAll();
	}

}