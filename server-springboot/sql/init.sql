-- 初始化数据库表
drop table if exists `canteen`, `store`, `user`;

create table `canteen`
(
	`id`            int unsigned auto_increment,
	`name`          varchar(16) not null,
	`init_score`    int         not null,
	`average_price` int         not null,
	`longitude`     double      not null,
	`latitude`      double      not null,
	`gmt_created`   datetime,
	`gmt_modified`  datetime,
	primary key (`id`)
) engine = InnoDB
  default charset = utf8mb4;

create table `store`
(
	`id`           int unsigned auto_increment,
	`name`         varchar(16) not null,
	`type`         varchar(8)  not null,
	`canteen_id`   int         not null,
	`gmt_created`  datetime,
	`gmt_modified` datetime,
	primary key (`id`)
) engine = InnoDB
  default charset = utf8mb4;

create table `user`
(
	`id`                  int unsigned auto_increment,
	`username`            varchar(16) not null unique,
	`password`            varchar(64) not null,
	`nickname`            varchar(32) not null,
	`canteen_record_json` text        not null,
	`type_record_json`    text        not null,
	`gmt_created`         datetime default now(),
	`gmt_modified`        datetime default now(),
	primary key (`id`)
) engine = InnoDB
  default charset = utf8mb4;

-- 插入初始数据
insert into `canteen` (`name`, `init_score`, `average_price`, `longitude`, `latitude`, `gmt_created`, `gmt_modified`)
values ('东苑食堂', 15, 8, 118.720039, 32.206388, now(), now()),
	   ('教职工食堂', 9, 8, 118.716412, 32.204707, now(), now()),
	   ('中苑老食堂', 14, 11, 118.716182, 32.202445, now(), now()),
	   ('中苑新食堂', 24, 12, 118.7143, 32.200558, now(), now()),
	   ('西苑老食堂（智慧食堂）', 22, 15, 118.706097, 32.203306, now(), now()),
	   ('西苑新食堂', 16, 15, 118.706074, 32.204013, now(), now());

insert into `store` (`name`, `type`, `canteen_id`, `gmt_created`, `gmt_modified`)
values ('一楼快餐', '简约快餐', 0, now(), now()),
	   ('二楼快餐', '简约快餐', 0, now(), now()),
	   ('三楼快餐', '简约快餐', 0, now(), now()),
	   ('闫阿妈', '套餐饭', 0, now(), now()),
	   ('阿峰铁板', '套餐饭', 0, now(), now()),
	   ('威斯特西餐', '套餐饭', 0, now(), now()),
	   ('石锅饭', '套餐饭', 0, now(), now()),
	   ('黄焖鸡米饭', '套餐饭', 0, now(), now()),
	   ('米线', '汤面或拌面', 0, now(), now()),
	   ('二楼面馆', '汤面或拌面', 0, now(), now()),
	   ('二楼馄饨', '馄饨水饺', 0, now(), now()),
	   ('淮扬美食', '炒菜炖菜类', 0, now(), now()),
	   ('鱼片炖菜', '炒菜炖菜类', 0, now(), now()),
	   ('二楼冒菜', '麻辣烫', 0, now(), now()),
	   ('三楼冒菜', '麻辣烫', 0, now(), now()),
	   ('二楼炸鸡汉堡', '炸鸡汉堡', 0, now(), now()),
	   ('三楼炸鸡汉堡', '炸鸡汉堡', 0, now(), now()),
	   ('一楼快餐', '简约快餐', 1, now(), now()),
	   ('大碗皮肚面', '汤面或拌面', 1, now(), now()),
	   ('烤鱼饭', '炒菜炖菜', 1, now(), now()),
	   ('一楼快餐', '简约快餐', 2, now(), now()),
	   ('二楼快餐', '简约快餐', 2, now(), now()),
	   ('中老闫阿妈', '套餐饭', 2, now(), now()),
	   ('小香村味道', '套餐饭', 2, now(), now()),
	   ('广式烧腊', '套餐饭', 2, now(), now()),
	   ('瓦香鸡米饭', '套餐饭', 2, now(), now()),
	   ('牛蛙', '套餐饭', 2, now(), now()),
	   ('二楼炒饭', '炒面或炒饭', 2, now(), now()),
	   ('二楼牛肉面', '汤面或拌面', 2, now(), now()),
	   ('一楼面条', '汤面或拌面', 2, now(), now()),
	   ('马姥姥水饺', '馄饨水饺', 2, now(), now()),
	   ('二楼麻辣烫', '麻辣烫', 2, now(), now()),
	   ('小匡米', '炸鸡汉堡', 2, now(), now()),
	   ('中式快餐', '简约快餐', 3, now(), now()),
	   ('经济营养快餐', '简约快餐', 3, now(), now()),
	   ('日韩料理', '套餐饭', 3, now(), now()),
	   ('蛋包饭', '套餐饭', 3, now(), now()),
	   ('寻味', '套餐饭', 3, now(), now()),
	   ('铁板厨房', '套餐饭', 3, now(), now()),
	   ('香酥鸡', '套餐饭', 3, now(), now()),
	   ('炖炖香木桶', '套餐饭', 3, now(), now()),
	   ('石锅菜', '套餐饭', 3, now(), now()),
	   ('阿姨炒饭', '炒面或炒饭', 3, now(), now()),
	   ('扬州炒饭', '炒面或炒饭', 3, now(), now()),
	   ('皮肚面', '汤面或拌面', 3, now(), now()),
	   ('重庆川味面', '汤面或拌面', 3, now(), now()),
	   ('淮南牛肉汤', '汤面或拌面', 3, now(), now()),
	   ('好味小面', '汤面或拌面', 3, now(), now()),
	   ('鲜约水饺', '馄饨水饺', 3, now(), now()),
	   ('甲味轩', '馄饨水饺', 3, now(), now()),
	   ('豆花鱼', '炒菜炖菜类', 3, now(), now()),
	   ('美食园小炒', '炒菜炖菜类', 3, now(), now()),
	   ('浓汁烧', '炒菜炖菜类', 3, now(), now()),
	   ('小灶王', '炒菜炖菜类', 3, now(), now()),
	   ('鸡鸣汤包', '炒菜炖菜类', 3, now(), now()),
	   ('一楼冒菜', '麻辣烫', 3, now(), now()),
	   ('老山宗麻辣香锅', '麻辣烫', 3, now(), now()),
	   ('二楼汉堡', '炸鸡汉堡', 3, now(), now()),
	   ('一楼快餐', '简约快餐', 4, now(), now()),
	   ('二楼快餐', '简约快餐', 4, now(), now()),
	   ('三楼快餐', '简约快餐', 4, now(), now()),
	   ('超级炒肉饭', '套餐饭', 4, now(), now()),
	   ('瓦香鸡', '套餐饭', 4, now(), now()),
	   ('香煎鸡扒饭', '套餐饭', 4, now(), now()),
	   ('烤盘饭', '套餐饭', 4, now(), now()),
	   ('牛肉饭', '套餐饭', 4, now(), now()),
	   ('三楼炒面', '炒面或炒饭', 4, now(), now()),
	   ('重庆小面', '汤面或拌面', 4, now(), now()),
	   ('过桥米线', '汤面或拌面', 4, now(), now()),
	   ('二楼水饺', '馄饨水饺', 4, now(), now()),
	   ('无骨烤鱼饭', '炒菜炖菜类', 4, now(), now()),
	   ('黄焖鸡米饭', '炒菜炖菜类', 4, now(), now()),
	   ('三楼麻辣烫', '麻辣烫', 4, now(), now()),
	   ('二楼炸鸡汉堡', '炸鸡汉堡', 4, now(), now()),
	   ('三楼炸鸡汉堡', '炸鸡汉堡', 4, now(), now()),
	   ('一楼快餐', '简约快餐', 5, now(), now()),
	   ('土耳其烤肉饭', '套餐饭', 5, now(), now()),
	   ('黄焖鸡米饭', '套餐饭', 5, now(), now()),
	   ('二楼炒饭', '炒面或炒饭', 5, now(), now()),
	   ('牛肉面', '汤面或拌面', 5, now(), now()),
	   ('重庆小面', '汤面或拌面', 5, now(), now()),
	   ('水饺', '馄饨水饺', 5, now(), now()),
	   ('麻辣烫', '麻辣烫', 5, now(), now()),
	   ('韩式炸鸡', '炸鸡汉堡', 5, now(), now());